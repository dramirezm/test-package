import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HeaderTest extends StatelessWidget{

  HeaderTest();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildActions(context),
              _buildUser(context),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildUser(BuildContext context){
    return Container(
      margin: EdgeInsets.only(top:24.0,left: 20, right: 16,bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Welcome"),
          SizedBox(height: 10),
        ],
      ),
    );
  }



  Widget _buildActions(BuildContext context){
    return Container(
        margin: EdgeInsets.only(right: 20,bottom: 48),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                  onTap: null,
                  child: Icon(Icons.settings_rounded)),
              // SizedBox(width: 16),
              // Icon(Icons.notification_important,color: Colors.white)
            ])
    );
  }


}